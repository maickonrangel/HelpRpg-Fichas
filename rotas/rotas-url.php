<?php
define('CLASSPATH', "class/");
define('JSPATH', "js/");
define('CSSPATH', "css/");
define('IMGPATH', "img/");
define('HOMEPATH', "pages/home");

//Definindo constantes para os caminhos base
define('ROOTPATH',"C:/xampp/htdocs/HelpRpg/"); 			
define('ROOTPATHURL',"http://127.0.0.1/HelpRpg/");
define('BASE_PATH', 'http://127.0.0.1/HelpRpg-Fichas/'); //http://helprpg.com.br
define('BASE_DISQUS_PATH', 'http://helprpg.com.br');

define('SEARCHPATH',ROOTPATHURL.'pages/search/');
define('LIBPATH', ROOTPATHURL."lib/");
define('CKEDITORPATH', ROOTPATHURL."lib/ckeditor/");
define('ABOUTPATH', ROOTPATHURL."pages/sobre/");
define('HOWTOUSE', ROOTPATHURL.'pages/como-usar/');
define('DOWNLOADPATH','arquivos/');
define('NPCGENERATEPATH',ROOTPATHURL.'old/index.php?p=fichas');
define('MONSTERGENERATEPATH',ROOTPATHURL.'old/index.php?p=monstros');
define('ROLLDICE',ROOTPATHURL.'roll-dice');
define('DUNGEONPATH',ROOTPATHURL.'pages/masmorras/');
define('MAPWORDPATH',ROOTPATHURL.'pages/mapa_mundi_fantasia/');
define('NAMEGENERATION',ROOTPATHURL.'pages/nomes/');
define('ADVENTUREGENERATION',ROOTPATHURL.'pages/gerador-aventuras/');
define('FICHASBASEGENERATION',ROOTPATHURL.'pages/ficha-base/');
define('CARACTERISTICASGENERATION',ROOTPATHURL.'pages/caracteristicas/');
define('DOWNLOADPATHMASMORRAS',ROOTPATHURL."pages/downloads/masmorras/");
define('DOWNLOADPATHMAPASMUNDI',ROOTPATHURL."pages/downloads/mapas-mundi/");
define('DOWNLOADPATHFICHAS',ROOTPATHURL."pages/downloads/fichas/");
define('DOWNLOADPATHITENS',ROOTPATHURL."pages/downloads/itens/");
define('DOWNLOADPATHMAGIAS',ROOTPATHURL."pages/downloads/magias/");

//url midias sociais
define('YOUTUBE_URL','https://www.youtube.com/channel/UCVx62ydCm9D9JubvsEQXwlA');
define('FACEBOOK_URL','https://www.facebook.com/Help-RPG-539011612872186/?ref=hl');
define('WORDPRESS_BLOG_URL','https://helprpg.wordpress.com/');
define('TAVERNA_DO_ELFO_URL','http://tavernadoelfo.com.br/');
define('BIBLIOTECA_ELFICA_URL','http://www.bibliotecaelfica.com/');

//url login
define('LOGINPATH', ROOTPATHURL."pages/login");

//URL - Usuarios
define('USERPATH',ROOTPATHURL."pages/usuario/");
define('USERSLISTPATH',ROOTPATHURL."pages/usuario/users.php");
define('USERNEWPATH',ROOTPATHURL."pages/usuario/new.php");
define('USEREDITPATH',ROOTPATHURL."pages/usuario/edit.php");
define('USERDELETEPATH',ROOTPATHURL."pages/usuario/delete.php");
define('USEREVIEWPATH',ROOTPATHURL."pages/usuario/view.php");
define('USERPASSPATH',ROOTPATHURL."pages/usuario/pass.php");

//File - Links de arquivos do sistemas
define('LOGIN_PATH', ROOTPATHURL.'pages/login/');
define('LOGIN_VALIDATION_PATH', ROOTPATHURL.'pages/login/login.php');
define('LOGOFF_PATH', 'pages/login/logoff.php');
define('HOME_PATH', ROOTPATHURL.'pages/home/');
define('PAINEL_PATH', ROOTPATHURL.'pages/painel/');
