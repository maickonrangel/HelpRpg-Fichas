<?php

//Nome/Titulo do site/projeto
define('PROJECTTITLE', "Help RPG Fichas");
define('PROGRAMER', 'Desenvolvido por <a href="http://www.mrcurriculo.orgfree.com/" target="blank">Maickon Rangel</a> todos os direitos reservados');
define('COPY', "&copy 2013-".date('Y'));

//Labels de menu no painel adm e msg de alerta
define('NOVO', "Novo");
define('EDITAR', "Editar");
define('DELETAR', "Apagar");
define('VER', "Visualizar");
define("BACK", "Voltar");
define('SUCESSO_MSG', "Registro deletado com sucesso.");
define('PERIGO_MSG', "Um erro ocoreu, contate o administrador.");

define('VOCE', "Você");
define('SAIR', "Sair");
define('LOGAR', 'Logar');
define('USUARIOS', "Usuários");
define('ESTATISTICAS', "Estatísticas");
define('SEUS_DADOS', "Seus dados");
define('EDITAR_DADOS', "Editar dados");
define('CANCELAR_CONTA', "Cancelar conta");
define('PERSONAGEM_MENU', "Personagem");
define('CRIAR_PERSONAGEM', "Criar um personagem");
define('CRIAR_MONSTRO', "Criar um monstro");
define('CRIAR_BOSS', "Criar um BOSS");
define('SISTEMA_DE_JOGO', "Sistema de jogo");
define('FICHA_DE_PERSONAGEM', "Fichas de Personagem");
define('FICHA_DE_NPC', "Fichas de Npc");
define('FICHA_DE_MONSTRO', "Fichas de Monstros");
define('FICHA_ALEATORIA', "Ficha Aleatória");
define('MONSTRO_ALEATORIO', "Monstro Aleatório");

define('UTILITARIOS','Utilitários');
define('ROLAR_DADOS','Rolar Dados');
define('GERADOR_DE_MUNDOS','Gerador de Mundos');
define('GERADOR_DE_NOMES','Gerador de Nomes');
define('GERADOR_DE_AVENTURAS','Gerador de Aventuras');
define('GERADOR_DE_CARACTERISTICAS','Gerador de Características');
define('GERADOR_DE_MASMORRAS','Gerador de Masmorras');
define('GERADOR_DE_FICHAS_BASE','Gerador de ficha base');
define('GERADOR_DE_NPCS','Gerador de Npcs');
define('GERADOR_DE_MONSTROS','Gerador de Monstros');
define('DOWNLOADS','Downloads');
define('DOWNLOAD','Baixar');
define('CAMPANHAS','Campanhas');

define('BESTIARIO', "Bestiário");
define('CADASTRAR', "Cadastrar");
define('AVENTURAS', "Aventuras");
define('CENARIOS', "Cenários");
define('CENARIO', "Cenário");
define('CONTOS', "Contos");
define('CRONICAS', "Crônicas");
define('CRONICAS_DESCRICAO', "Crônica completa");
define('CONTO', "Conto");
define('HISTORIAS_PAGINA', "Histórias");
define('EDITADO_POR', "Editado por");

//midias sociais
define('MEDIAS_SOCIAIS', "Mídias Sociais");
define('FACEBOOK', "Página no Facebook");
define('YOUTUBE', "Canal no Youtube");
define('WORDPRESS_BLOG', "Blog Help Rpg");

//sites parceiros
define('TAVERNA_DO_ELFO', "Taverna do Elfo");
define('BIBLIOTECA_ELFICA', "Biblioteca Elfica");

//app
define('MASMORRAS',"Mapas Masmorras");
define('MAPAS', "Mapas Mundi");
define('FICHAS', "Fichas");
define('ITENS', "Itens");
define('MAGIAS_MENU', "Magias");

//paginas
define('HOME','Home');
define('CADASTRO','Cadastro');
define('SOBRE','Sobre');
define('COMO_USAR','Como Usar');

//termos pagina aventuras
define("TITULO", "Título");
define("MESTRE", "Mestre");
define('LEVEL_INDICADO', "Nível indicado");
define("TIPO", "Tipo");
define("CRIADO_EM", "Criado em");
define("SISTEMA", "Sistema");
define("CRIADOR_MENU", "Criado por");
define("NIVEL_INDICADO_AVENTURA", "Nível indicado para a aventura");
define('MESTRE_DA_AVENTURA', "Mestre da aventura");
define('TIPO_DE_AVENTURA', "Tipo de aventura");
define('CLASSIFICACAO', "classificação");
//define('SISTEMA_DE_RPG', "Sistema de RPG");
define('HISTORIA_DA_AVENTURA', "História da aventura");
define('CRIADO_NO_DIA', "Criado no dia");
define('CADASTRADO_POR', "Cadastrado por");
define('AVENTURA_INDICADA_NIVEL', "Aventura indicada para o nível");
//termos pagina contos
define('AUTOR', "Autor");
define('DONO', "Dono");
define('DESCRICAO_BREVE', "Descrição breve");
//pagina upload de fichas
define('URL_ARQUIVO', 'URL do arquivo');
define('UPLOAD_FILE', 'Upload de arquivo de ficha');

define('USUARIOS_CADASTRADOS', "Usuários Cadastrados");
define('MONSTROS_CADASTRADOS', "Monstros cadastrados");
define('BESTIARIO_CADASTRADOS', "Bestiário cadastrados");
define('CHEFES_DE_FASE', "Chefes de fase");
define('ITENS_CADASTRADOS', "Itens Cadastrados");

define('MSG_BEM_VINDO_INDEX', "O que você procura está aqui no");
define('MSG_TEXT_INDEX_01', "Contribua com o Help Rpg, faça uma doação para motivar a evolução deste site a assim termos um ambiente cada vez 
melhor para nossas campanhas de RPG. Contamos com você.");

define('REGISTRO_NAO_ENCONTRADO_MSG', 'Este registro não existe em nossa base de dados. Faça uma conta <a href="'.USERNEWPATH.'">aqui </a>e crie novos registros em nossa base de dados.');