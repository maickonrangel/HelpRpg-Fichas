<?php
define('PROJECTTITLE', "Help RPG Sheets");
define('PROGRAMER', 'Development by <a href="http://www.mrcurriculo.orgfree.com/" target="blank">Maickon Rangel</a> All rights reserved.');
define('COPY', "&copy 2013-2015");

//Menu labels from menage pane and msg alert
define('NOVO', "New");
define('EDITAR', "Edit");
define('DELETAR', "Delete");
define('VER', "View");
define('DOWNLOAD','Download');
define("BACK", "Back");
define('SUCESSO_MSG', "Successfully deleted record.");
define('PERIGO_MSG', "An error occurred, contact your administrator.");

define('VOCE', "You");
define('SAIR', "Logout");
define('LOGAR', 'Login');
define('USUARIOS', "Users");
define('ESTATISTICAS', "Statistics");
define('SEUS_DADOS', "Your data");
define('EDITAR_DADOS', "Edit data");
define('CANCELAR_CONTA', "Cancel account");
define('PERSONAGEM_MENU', "Character");
define('CRIAR_PERSONAGEM', "New Character");
define('CRIAR_MONSTRO', "New Monster");
define('CRIAR_BOSS', "New BOSS");
define('SISTEMA_DE_JOGO', "Game system");
define('FICHA_DE_PERSONAGEM', "Character Sheets");
define('FICHA_DE_NPC', "Sheets NPC");
define('FICHA_DE_MONSTRO', "Sheets Monsters");
define('FICHA_ALEATORIA', "Random character");
define('MONSTRO_ALEATORIO', "Random monsters");

define('UTILITARIOS','Utilities');
define('ROLAR_DADOS','Roll Dice');
define('GERADOR_DE_MUNDOS','New world');
define('GERADOR_DE_NOMES','Name Generator');
define('GERADOR_DE_AVENTURAS','Adventure Generator');
define('GERADOR_DE_CARACTERISTICAS','Features of the Generator');
define('GERADOR_DE_MASMORRAS','Dungeon Generator');
define('GERADOR_DE_FICHAS_BASE','Based chips generator');
define('GERADOR_DE_NPCS','Npc Generator');
define('GERADOR_DE_MONSTROS','Monster Generator');
define('DOWNLOADS','Downloads');
define('CAMPANHAS','Campaigns');

define('BESTIARIO', "Bestiary");
define('CADASTRAR', "Register");
define('AVENTURAS', "Adventures");
define('CENARIOS', "Scenario");
define('CENARIO', "Scenario");
define('CONTOS', "Tales");
define('CRONICAS', "Chronic");
define('CRONICAS_DESCRICAO', "Full chronic");
define('HISTORIAS_PAGINA', "Stories");
define('EDITADO_POR', "Edited by");

//social medias
define('MEDIAS_SOCIAIS', "Social media");
define('FACEBOOK', "Facebook");
define('YOUTUBE', "Youtube");
define('WORDPRESS_BLOG', "Blog Help Rpg");

//sites parceiros
define('TAVERNA_DO_ELFO', "Elf Tavern");
define('BIBLIOTECA_ELFICA', "Elf Library");

//app
define('MASMORRAS',"Dungeons maps");
define('MAPAS', "World Maps");
define('FICHAS', "Fichas");
define('ITENS', "Sheets");
define('MAGIAS_MENU', "spells");

//terms to page advetures
define("TITULO", "Ttitle");
define("MESTRE", "Master");
define('LEVEL_INDICADO', "Indicate Lv");
define("TIPO", "Type");
define("CRIADO_EM", "Create at");
define("SISTEMA", "System");
define("CRIADOR_MENU", "Create by");
define("NIVEL_INDICADO_AVENTURA", "Level indicated for adventure");
define('MESTRE_DA_AVENTURA', "Adventure Master");
define('TIPO_DE_AVENTURA', "Kind of adventure");
define('CLASSIFICACAO', "Classification");
define('SISTEMA_DE_RPG', "RPG system");
define('HISTORIA_DA_AVENTURA', "Adventure story");
define('CRIADO_NO_DIA', "Created on");
define('CADASTRADO_POR', "Joined by");

//pages
define('HOME','Home');
define('CADASTRO','Register');
define('SOBRE','About');
define('COMO_USAR','How To Use');

//terms to page tales
define('AUTOR', "Author");
define('DONO', "Owner");
define('DESCRICAO_BREVE', "brief description");

//File upload of shets
define('URL_ARQUIVO', 'File URL');
define('UPLOAD_FILE', 'Record file upload');

define('USUARIOS_CADASTRADOS', "Registered Users");
define('MONSTROS_CADASTRADOS', "Registered monsters");
define('BESTIARIO_CADASTRADOS', "Registered bestiary");
define('CHEFES_DE_FASE', "Phase Heads");
define('ITENS_CADASTRADOS', "Registrants items");

define('MSG_BEM_VINDO_INDEX', "What you seek is here at");
define('MSG_TEXT_INDEX_01', "Contribute to Help RPG , make a donation to motivate the development of this site as well have a environment each time
best for our RPG campaigns . We count on you .");
define('REGISTRO_NAO_ENCONTRADO_MSG', 'This record does not exist in our database. Make a account <a href="'.USERNEWPATH.'">here </a>
and create new records in our database.');